-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

local scratch = require("awesome-scratch.scratch")

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
package.loaded["naughty.dbus"] = {}
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
local theme = {}
theme.name = "default"
if (os.getenv("XDG_CONFIG_HOME")) then
  config_dir = os.getenv("XDG_CONFIG_HOME")
else
  config_dir = string.format("%s/.config", os.getenv("HOME"))
end
theme.dir = string.format("%s/awesome/themes/%s", config_dir, theme.name)
beautiful.init(theme.dir .. "/theme.lua")
beautiful.useless_gap = 5
beautiful.wallpaper = awful.util.get_configuration_dir() .. "../../background.png"

-- Widget definitions/initializations

local battery_widget = require("awesome-wm-widgets.batteryarc-widget.batteryarc")
local mpd_widget = require("awesome-wm-widgets.mpdarc-widget.mpdarc")
local volume_widget = require("awesome-wm-widgets.volume-widget.volume")
local ram_widget = require("awesome-wm-widgets.ram-widget.ram-widget")
local cpu_widget = require("awesome-wm-widgets.cpu-widget.cpu-widget")
local brightness_widget = require("awesome-wm-widgets.brightness-widget.brightness")
local net_speed = require("awesome-wm-widgets.net-speed-widget.net-speed")

-- Disable keybindings

local inertmode = false

-- Spawn windows at bottom of stack

local spawnatbottom = true


-- This is used later as the default terminal and editor to run.
terminal = "st"
editor = os.getenv("EDITOR") or "nvim"
editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod1"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    --awful.layout.suit.floating,
    awful.layout.suit.tile,
    --awful.layout.suit.tile.left,
    --awful.layout.suit.tile.bottom,
    --awful.layout.suit.tile.top,
    --awful.layout.suit.fair,
    --awful.layout.suit.fair.horizontal,
    --awful.layout.suit.spiral,
    --awful.layout.suit.spiral.dwindle,
    --awful.layout.suit.max,
    --awful.layout.suit.max.fullscreen,
    --awful.layout.suit.magnifier,
    --awful.layout.suit.corner.nw,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
}
-- }}}

-- Config function definitions {{{

function crcparse(substr)
  local file=assert(io.open(config_dir .. "/computerrc","r"))
  local line = ""
  repeat
    line=file:read("*line")
    if line and line:find(string.format("^%s=",substr)) then
      return string.sub(line,#substr + 2,-1)
    end
  until not line
end

function is_linux ()
  return crcparse("OS") == "Linux"
end

function is_obsd ()
  return crcparse("OS") == "OpenBSD"
end

function terminal_sp_cmd (spc, cmd)
  if terminal == "st" then
    cflag = "-c"
    gflag = "-g"
    xflag = "-e"
    geo = "100x30"
  elseif terminal == "alacritty" then
    cflag = "--class"
    gflag = false
    xflag = "-e"
  end
  if gflag then
    return terminal .. " " .. cflag .. " " .. spc .. " " .. gflag .. " " .. geo .. " " .. xflag .. " " .. cmd
  else
    return terminal .. " " .. cflag .. " " .. spc .. " " .. " " .. xflag .. " " .. cmd
  end
end

-- }}}

-- {{{ Menu
-- Create a launcher widget and a main menu
myawesomemenu = {
   { "hotkeys", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end },
   { "manual", terminal .. " -e man awesome" },
   { "edit config", editor_cmd .. " " .. awesome.conffile },
   { "restart", awesome.restart },
   { "quit", function() awesome.quit() end },
}

mymainmenu = awful.menu({ items = { { "awesome", myawesomemenu, beautiful.awesome_icon },
                                    { "open terminal", terminal }
                                  }
                        })

mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
                                     menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Wibar
-- Create a textclock widget
mytextclock = wibox.widget.textclock()

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
                )

local tasklist_buttons = gears.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  c:emit_signal(
                                                      "request::activate",
                                                      "tasklist",
                                                      {raise = true}
                                                  )
                                              end
                                          end),
                     awful.button({ }, 3, function()
                                              awful.menu.client_list({ theme = { width = 250 } })
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                          end))

-- local function set_wallpaper(s)
    -- -- Wallpaper
    -- if beautiful.wallpaper then
        -- local wallpaper = beautiful.wallpaper
        -- -- If wallpaper is a function, call it with the screen
        -- if type(wallpaper) == "function" then
            -- wallpaper = wallpaper(s)
        -- end
        -- gears.wallpaper.maximized(wallpaper, s, true)
    -- end
-- end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
-- screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(s)
    -- -- Wallpaper
    -- set_wallpaper(s)

    -- Each screen has its own tag table.
    awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, s, awful.layout.layouts[1])

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        buttons = taglist_buttons
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.focused,
        buttons = tasklist_buttons
    }

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s, height = 24})

    if (crcparse("snd") == "pipewire") or (crcparse("snd") == "pulseaudio") or (crcparse("snd") == "pulse") then
      snd="pulse"
    else
      snd="default"
    end

    net = crcparse("net")

    if is_linux() then
      -- Add widgets to the wibox
      s.mywibox:setup {
          layout = wibox.layout.align.horizontal,
          { -- Left widgets
              layout = wibox.layout.fixed.horizontal,
              mylauncher,
              s.mytaglist,
              s.mypromptbox,
          },
          s.mytasklist, -- Middle widget
          {
              layout = wibox.layout.fixed.horizontal,
              mpd_widget,
              net_speed {
                interface = net
              },
              ram_widget(),
              cpu_widget(),
              brightness_widget {
                program = "light",
                path_to_icon = "/usr/share/icons/Paper/scalable/status/display-brightness-symbolic.svg", 
                step = 5,
                timeout = 1,
              },
              volume_widget {
                path_to_icon = "/usr/share/icons/Paper/scalable/status/audio-volume-muted-symbolic.svg",
                widget_type = "arc",
                mute_color = "#ff111111",
                device = snd,
              },
              battery_widget { path_to_icons = "/usr/share/icons/Paper/scalable/status/" },
              mykeyboardlayout,
              wibox.widget.systray(),
              mytextclock,
              s.mylayoutbox,
          },
      }
    elseif is_obsd() then
       -- Add widgets to the wibox
      s.mywibox:setup {
          layout = wibox.layout.align.horizontal,
          { -- Left widgets
              layout = wibox.layout.fixed.horizontal,
              mylauncher,
              s.mytaglist,
              s.mypromptbox,
          },
          s.mytasklist, -- Middle widget
          {
              layout = wibox.layout.fixed.horizontal,
              mpd_widget,
              mykeyboardlayout,
              wibox.widget.systray(),
              mytextclock,
              s.mylayoutbox,
          },
      }
    end
  end)
  
-- }}}

-- {{{ Mouse bindings
root.buttons(gears.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))

client.connect_signal("mouse::enter", function(c)
  c:emit_signal("request::activate", "mouse_enter", {raise = false})
end)
-- }}}

-- {{{ Key bindings


globalkeys = gears.table.join(
    -- scratchpads
    awful.key({ modkey, "Control" }, "z", function ()
      scratch.toggle(terminal_sp_cmd("sphtop","htop"), { class = "sphtop" })
      awful.placement.centered()
    end,
              {description = "Toggle htop scratchpad"}),
    awful.key({ modkey, "Control" }, "x", function ()
      scratch.toggle(terminal_sp_cmd("spterm","zsh"), { class = "spterm" })
      awful.placement.centered()
    end,
              {description = "Toggle term scratchpad"}),
    awful.key({ modkey, "Control" }, "c", function ()
      scratch.toggle(terminal_sp_cmd("sppmxr","pulsemixer"), { class = "sppmxr" })
      awful.placement.centered()
    end,
              {description = "Toggle pulsemixer scratchpad"}),
    awful.key({ modkey, "Control" }, "v", function ()
      scratch.toggle(terminal_sp_cmd("spblue","bluetoothctl"), { class = "spblue" })
      awful.placement.centered()
    end,
              {description = "Toggle bluetoothctl scratchpad"}),
    awful.key({ modkey, "Control" }, "b", function ()
      scratch.toggle(terminal_sp_cmd("spncmp","ncmpcpp"), { class = "spncmp" })
      awful.placement.centered()
    end,
              {description = "Toggle ncmpcpp scratchpad"}),
    awful.key({ modkey, "Control" }, "a", function ()
      scratch.toggle(terminal_sp_cmd("spmutt","neomutt"), { class = "spmutt" })
      awful.placement.centered()
    end,
              {description = "Toggle neomutt scratchpad"}),
    awful.key({ modkey, "Control" }, "s", function ()
      scratch.toggle(terminal_sp_cmd("spprof","profanity"), { class = "spprof" })
      awful.placement.centered()
    end,
              {description = "Toggle profanity scratchpad"}),
    awful.key({ modkey, "Control" }, "d", function ()
      scratch.toggle(terminal_sp_cmd("spircc","irssi"), { class = "sptrem" })
      awful.placement.centered()
    end,
              {description = "Toggle irssi scratchpad"}),
    awful.key({ modkey, "Control" }, "f", function ()
      scratch.toggle(terminal_sp_cmd("sptodo","todo"), { class = "sptrem" })
      awful.placement.centered()
    end,
              {description = "Toggle todo.txt scratchpad"}),
    awful.key({ modkey, "Control" }, "g", function ()
      scratch.toggle(terminal_sp_cmd("sptrem","tremc"), { class = "sptrem" })
      awful.placement.centered()
    end,
              {description = "Toggle tremc scratchpad"}),
    awful.key({ modkey,           }, "a", function ()
        spawnatbottom = not spawnatbottom
    end,
              {description = "toggle attach at bottom", group = "layout"}),
    awful.key({ modkey,           }, "b", function ()
      root.keys(inertkeys)
    end,
              {description = "turn inert mode on", group = "awesome"}),
    --awful.key({ modkey,           }, "s", function () 
        --hotkeys_popup.show_help()
    --end,
              --{description="show help", group="awesome"}),
    --awful.key({ modkey,           }, "Left",   function () 
        --awful.tag.viewprev()
    --end,
              --{description = "view previous", group = "tag"}),
    --awful.key({ modkey,           }, "Right",  function () 
        --awful.tag.viewnext()
    --end,
              --{description = "view next", group = "tag"}),
    awful.key({modkey,            }, "Return", function ()
        awful.spawn.with_shell("st")
      end,
              {description = "Spawn terminal", group = "spawn"}),
    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
        end,
        {description = "focus next by index", group = "client"}),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
        end,
        {description = "focus previous by index", group = "client"}),
    awful.key({ modkey,           }, "w", function ()
        mymainmenu:show()
      end,
              {description = "show main menu", group = "awesome"}),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end,
              {description = "swap with next client by index", group = "client"}),
    awful.key({ modkey, "Shift"   }, "k", function ()
        awful.client.swap.byidx( -1)   
      end,
              {description = "swap with previous client by index", group = "client"}),
    awful.key({ modkey, "Control" }, "j", function ()
        awful.screen.focus_relative( 1)
      end,
              {description = "focus the next screen", group = "screen"}),
    awful.key({ modkey, "Control" }, "k", function ()
        awful.screen.focus_relative(-1)
      end,
              {description = "focus the previous screen", group = "screen"}),
    -- awful.key({ modkey,           }, "u", function ()
        -- awful.client.urgent.jumpto()
    -- end,
              -- {description = "jump to urgent client", group = "client"}),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end,
        {description = "go back", group = "client"}),

    -- Standard program
--    awful.key({ modkey,           }, "Return", function ()
--    if (not inertmode) then
--    awful.spawn(terminal)
--    end
--    end,
--              {description = "open a terminal", group = "launcher"}),
    awful.key({ modkey, "Shift"   }, "r", function ()
        awesome.restart()
    end,
              {description = "reload awesome", group = "awesome"}),
    awful.key({ modkey, "Shift"   }, "e", function ()
        awesome.quit()
    end,
              {description = "quit awesome", group = "awesome"}),

    awful.key({ modkey,           }, "l",     function ()
        awful.tag.incmwfact( 0.05)
    end,
              {description = "increase master width factor", group = "layout"}),
    awful.key({ modkey,           }, "h",     function ()
        awful.tag.incmwfact(-0.05)
    end,
              {description = "decrease master width factor", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "h",     function ()
        awful.tag.incnmaster( 1, nil, true)
    end,
              {description = "increase te number of master clients", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "l",     function ()
        awful.tag.incnmaster(-1, nil, true)
    end,
              {description = "decrease the number of master clients", group = "layout"}),
    awful.key({ modkey, "Control" }, "h",     function ()
        awful.tag.incncol( 1, nil, true)   
    end,
              {description = "increase the number of columns", group = "layout"}),
    awful.key({ modkey, "Control" }, "l",     function ()
        awful.tag.incncol(-1, nil, true)   
    end,
              {description = "decrease the number of columns", group = "layout"}),
    awful.key({ modkey,           }, "space", function ()
    
        awful.layout.inc( 1)               
    end,
              {description = "select next", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "space", function ()
        awful.layout.inc(-1)               
      end,
              {description = "select previous", group = "layout"}),

    awful.key({ modkey, "Control" }, "n",
              function ()
                  local c = awful.client.restore()
                  -- Focus restored client
                  if c then
                    c:emit_signal(
                        "request::activate", "key.unminimize", {raise = true}
                    )
                  end
              end,
              {description = "restore minimized", group = "client"}),

    -- Prompt
--    awful.key({ modkey },            "r",     function () awful.screen.focused().mypromptbox:run() end,
--              {description = "run prompt", group = "launcher"}),

    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run {
                    prompt       = "Run Lua code: ",
                    textbox      = awful.screen.focused().mypromptbox.widget,
                    exe_callback = awful.util.eval,
                    history_path = awful.util.get_cache_dir() .. "/history_eval"
                  }
              end,
              {description = "lua execute prompt", group = "awesome"})
    -- Menubar
--  awful.key({ modkey }, "p", function() menubar.show() end,
--            {description = "show the menubar", group = "launcher"})
)

clientkeys = gears.table.join(
    awful.key({ modkey,           }, "f",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        {description = "toggle fullscreen", group = "client"}),
    awful.key({ modkey, "Shift"   }, "q",      function (c)
        c:kill()
    end,
              {description = "close", group = "client"}),
    awful.key({ modkey, "Control" }, "space",  function (c)
        c.floating = not c.floating
    end,
              {description = "toggle floating", group = "client"}),
    awful.key({ modkey, "Control" }, "Return", function (c)
        c:swap(awful.client.getmaster())
    end,
              {description = "move to master", group = "client"}),
    awful.key({ modkey,           }, "o",      function (c)
        c:move_to_screen()
    end,
              {description = "move to screen", group = "client"}),
    awful.key({ modkey,           }, "t",      function (c)
        c.ontop = not c.ontop
    end,
              {description = "toggle keep on top", group = "client"}),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end ,
        {description = "minimize", group = "client"}),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized = not c.maximized
            c:raise()
        end ,
        {description = "(un)maximize", group = "client"}),
    awful.key({ modkey, "Control" }, "m",
        function (c)
            c.maximized_vertical = not c.maximized_vertical
            c:raise()
        end ,
        {description = "(un)maximize vertically", group = "client"}),
    awful.key({ modkey, "Shift"   }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c:raise()
        end ,
        {description = "(un)maximize horizontally", group = "client"})
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = gears.table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = awful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end
                  end,
                  {description = "view tag #"..i, group = "tag"}),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = awful.screen.focused()
                      local tag = screen.tags[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end,
                  {description = "toggle tag #" .. i, group = "tag"}),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                      end
                  end,
                  {description = "move focused client to tag #"..i, group = "tag"}),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end,
                  {description = "toggle focused client on tag #" .. i, group = "tag"})
    )
end

clientbuttons = gears.table.join(
    awful.button({ 0      }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
    end),
    awful.button({ modkey }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.move(c)
    end),
    awful.button({ modkey }, 3, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.resize(c)
    end)
)

inertkeys = gears.table.join(
    awful.key({ modkey,           }, "b", function ()
      root.keys(globalkeys)
    end,
              {description = "turn inert mode off", group = "awesome"})
)

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     screen = awful.screen.preferred,
                     placement = awful.placement.no_overlap+awful.placement.no_offscreen
     }
    },

    -- Floating clients.
    { rule_any = {
        instance = {
          "DTA",  -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
          "pinentry",
        },
        class = {
          --"mpv",
          "sphtop",
          "spterm",
          "sppmxr",
          "spblue",
          "spncmp",
          "spmutt",
          "spprof",
          "spircc",
          "sptodo",
          "sptrem",
          "ProtonMail Bridge",
          "TheFiniteDemo",
          "Arandr",
          "Blueman-manager",
          "Gpick",
          "Kruler",
          "MessageWin",  -- kalarm.
          "Sxiv",
          "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
          "Wpa_gui",
          "veromix",
          "xtightvncviewer"},

        -- Note that the name property shown in xprop might be set slightly after creation of the client
        -- and the name shown there might not match defined rules here.
        name = {
          "Event Tester",  -- xev.
        },
        role = {
          "AlarmWindow",  -- Thunderbird's calendar.
          "ConfigManager",  -- Thunderbird's about:config.
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
      }, properties = { floating = true }},

    -- Add titlebars to normal clients and dialogs
    { rule_any = {type = { "normal", "dialog" }
      }, properties = { titlebars_enabled = true }
    },

    -- Set Firefox to always map on the tag named "2" on screen 1.
    -- { rule = { class = "Firefox" },
    --   properties = { screen = 1, tag = "2" } },
}
-- }}}

-- {{{ Window Swallowing functions

--[[
function is_terminal(c)
    return (c.class and (c.class:match("Alacritty") or c.class:match("St"))) and true or false
end

function copy_size(c, parent_client)
    if not c or not parent_client then
        return
    end
    if not c.valid or not parent_client.valid then
        return
    end
    c.x=parent_client.x;
    c.y=parent_client.y;
    c.width=parent_client.width;
    c.height=parent_client.height;
end
function check_resize_client(c)
    if(c.child_resize) then
        copy_size(c.child_resize, c)
    end
end

client.connect_signal("property::size", check_resize_client)
client.connect_signal("property::position", check_resize_client)
client.connect_signal("manage", function(c)
    if is_terminal(c) then
        return
    end
    local parent_client=awful.client.focus.history.get(c.screen, 1)
    if parent_client and is_terminal(parent_client) then
        parent_client.child_resize=c
        c.floating=true
        copy_size(c, parent_client)
    end
end)
--]]
--[[
client.connect_signal("manage", function(c)
    if is_terminal(c) then
        return
    end
    local parent_client=awful.client.focus.history.get(c.screen, 1)
    if parent_client and is_terminal(parent_client) then
        parent_client.child_resize=c
        parent_client.minimized = true

        c:connect_signal("unmanage", function() parent_client.minimized = false end)
        
        -- c.floating=true
        copy_size(c, parent_client)
    end
end)
--]]
--[[
function is_terminal(c)
    return (c.class and (c.class:match("Alacritty") or c.class:match("St"))) and true or false
end

-- swallow
client.connect_signal("manage", function(c)
  if is_terminal(c) then
    return
  end

  local parent_client=awful.client.focus.history.get(c.screen, 1)

  awful.spawn.easy_async('dash '..awful.util.get_configuration_dir()..'helper.sh gppid '..c.pid, function (gppid)
    awful.spawn.easy_async('dash '..awful.util.get_configuration_dir()..'helper.sh ppid '..c.pid, function(ppid)
      if parent_client and (gppid:find('^' .. parent_client.pid) or ppid:find('^' .. parent_client.pid))and is_terminal(parent_client) then
        parent_client.child_resize=c
        parent_client.minimized = true

        c:connect_signal("unmanage", function() parent_client.minimized = false end)

        if (c.floating) then
          copy_size(c, parent_client)
        end
      end
    end)
  end)
end)
--]]
-- }}}

-- {{{ Autostart
-- awful.spawn.with_shell("/usr/bin/sxhkd")
awful.spawn.with_shell(string.format("sh %s/autostart.sh &", config_dir))
-- }}}

-- spawn windows as slaves {{{
client.connect_signal(
    "manage",
    function(c)
        if not awesome.startup then
          if spawnatbottom then
            awful.client.setslave(c)
          end
        end
    end
)
-- }}}
